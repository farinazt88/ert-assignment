import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { CardHeader } from "@mui/material";
import TextareaAutosize from "@mui/base/TextareaAutosize";
import { Box } from "@mui/system";
import Grid from "@mui/material/Grid";

function createData(name, note, date) {
  return { name, note, date };
}

export default class Notes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: [
        createData("DR. x", "some note .........", "2021-08-02"),
        createData(
          "DR. y",
          "She have fever and sore throat.......",
          "2021-09-07"
        ),
        createData("DR. x", "some note here .......", "2022-01-04"),
      ],
      test: "hi",
    };
  }
  handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    let now = new Date();
    this.setState({
      rows: [
        ...this.state.rows,
        createData(
          "DR. x",
          data.get("note"),
          now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay()
        ),
      ],
    });
  };
  render() {
    return (
      <div>
        <Box
          component="form"
          noValidate
          onSubmit={this.handleSubmit}
          sx={{ m: 2 }}
        >
          <Grid container spacing={2} justifyContent="center">
            <Grid item>
              <TextareaAutosize
                aria-label="empty textarea"
                minRows={3}
                id="note"
                name="note"
                placeholder="Write down your note"
                style={{ width: 200 }}
              />
            </Grid>
            <Grid item>
              <Button type="submit" variant="contained" sx={{ mt: 3, mb: 2 }}>
                Send
              </Button>
            </Grid>
          </Grid>
        </Box>
        {this.state.rows.map((rows, i) => (
          <Card sx={{ m: 2 }}>
            <CardHeader title={rows.name} subheader={rows.date} />
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                {rows.note}
              </Typography>
            </CardContent>
          </Card>
        ))}
      </div>
    );
  }
}

import React, { Component } from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import PatientTable from "../components/patientsTable";

export default class adminPatients extends Component {
  render() {
    return (
      <div>
        <Grid item lg={12}>
          <Paper
            sx={{
              p: 0,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <PatientTable></PatientTable>
          </Paper>
        </Grid>
      </div>
    );
  }
}

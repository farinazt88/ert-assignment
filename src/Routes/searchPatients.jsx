import PatientTableCare from "../components/patientsTableCare";
import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import SearchIcon from "@mui/icons-material/Search";

function createData(
  patientName,
  birthDate,
  gender,
  healthNumber,
  insuranceProvider
) {
  return { patientName, birthDate, gender, healthNumber, insuranceProvider };
}
export default class SearchPatients extends React.Component {
  constructor(props) {
    super(props);
    this.selectHandler.bind(this);
    this.state = {
      by: "",
      selectedPatient: {},
      patients: [
        createData("Robert Yun", "February 25, 1994", "male", 59400849, "xy"),
        createData("Louie Neece", "June 22, 1942", "male", 29090333, "xy"),
        createData(
          "Mark R. Elizondo",
          "August 24, 1937",
          "male",
          79773249,
          "xy"
        ),
        createData("Wendy L. Moore", "May 4, 1976", "female", 56030360, "xy"),
        createData(
          "Rosalie Brooks",
          "August 14, 1992",
          "female",
          88230653,
          "xy"
        ),
      ],
    };
  }
  handleChange = (event) => {
    this.setState({ by: event.target.value });
  };
  selectHandler = (patient) => {
    this.setState({ selectedPatient: patient });
    localStorage.setItem("selectedPatient", patient);
  };

  render() {
    return (
      <div>
        <Box
          sx={{
            marginTop: 2,
            marginBottom: 2,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Grid
            container
            spacing={1}
            justifyContent="center"
            alignItems="center"
          >
            <Grid item>
              <b>Selected Patient :</b>{" "}
              {this.state.selectedPatient.patientName
                ? this.state.selectedPatient.patientName +
                  " / " +
                  this.state.selectedPatient.healthNumber
                : ""}
            </Grid>
            <Grid item lg={12}></Grid>
            <Grid item>
              <TextField id="searchInput" label="search" variant="outlined" />
            </Grid>
            <Grid item>
              <FormControl sx={{ minWidth: 100 }}>
                <InputLabel id="searchBy">by</InputLabel>
                <Select
                  labelId="searchBy"
                  id="searchBy"
                  value={this.state.by}
                  label="by"
                  onChange={this.handleChange}
                >
                  <MenuItem value={1}>Name</MenuItem>
                  <MenuItem value={2}>Health Card Number</MenuItem>
                  <MenuItem value={3}>Social Insurance Number</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <Button variant="contained" endIcon={<SearchIcon />}>
                search
              </Button>
            </Grid>
          </Grid>
        </Box>
        <PatientTableCare
          patients={this.state.patients}
          handler={this.selectHandler}
        />
      </div>
    );
  }
}

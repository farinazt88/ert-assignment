import React from "react";
import ReactDOM from "react-dom";
//import { BrowserRouter } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

//-----fonts
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
//-----icons

//

import App from "./App";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Routes/login";
import AdminLogin from "./Routes/adminLogin";
import AdminDashboard from "./Routes/adminDashboard";
import Dashboard from "./Routes/dashboard";
import Notes from "./Routes/notes";
import ContactDetails from "./Routes/contactDetails";
import AllergiesMeds from "./Routes/allergiesMeds";
import LabTestRes from "./Routes/labTestRes";
import Radiology from "./Routes/radiology";
import Billing from "./Routes/billing";
import AdminPatients from "./Routes/adminPatients";
import AdminStaff from "./Routes/adminStaff";
import AdminWelcome from "./components/adminWelcome.jsx";
import SearchPatients from "./Routes/searchPatients.jsx";
ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route element={<App />}>
        <Route path="/" element={<Login />}></Route>
        <Route path="/login" element={<Login />}></Route>

        <Route path="/admin" element={<AdminLogin />}></Route>
        <Route path="/admin/dashboard" element={<AdminDashboard />}>
          <Route exact path="" element={<AdminWelcome />}></Route>
          <Route path="patients" element={<AdminPatients />}></Route>
          <Route path="staff" element={<AdminStaff />}></Route>
        </Route>
        <Route path="/dashboard" element={<Dashboard />}>
          <Route exact path="" element={<SearchPatients />}></Route>
          <Route path="contactDetails" element={<ContactDetails />}></Route>
          <Route path="allergiesMeds" element={<AllergiesMeds />}></Route>
          <Route path="labTestRes" element={<LabTestRes />}></Route>
          <Route path="radiology" element={<Radiology />}></Route>
          <Route path="notes" element={<Notes />}></Route>
          <Route path="billing" element={<Billing />}></Route>
        </Route>
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>Page not found!!!!</p>
            </main>
          }
        />
      </Route>
    </Routes>
  </BrowserRouter>,

  document.getElementById("root")
);

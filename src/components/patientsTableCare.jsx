import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "rgba(0, 0, 0, 0.54)",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default class CustomizedPatients extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick.bind(this);
  }
  handleClick = () => {
    this.props.selectHandler("hey");
  };
  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell sx={{ bgcolor: "white" }}>
                Patient name
              </StyledTableCell>
              <StyledTableCell>Birth Date</StyledTableCell>
              <StyledTableCell>Gender</StyledTableCell>
              <StyledTableCell>Health Card Number</StyledTableCell>
              <StyledTableCell>Insurance Provider</StyledTableCell>
              <StyledTableCell></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.patients.map((row) => (
              <StyledTableRow key={row.patientName}>
                <StyledTableCell component="th" scope="row">
                  {row.patientName}
                </StyledTableCell>
                <StyledTableCell>{row.birthDate}</StyledTableCell>
                <StyledTableCell>{row.gender}</StyledTableCell>
                <StyledTableCell>{row.healthNumber}</StyledTableCell>
                <StyledTableCell>{row.insuranceProvider}</StyledTableCell>
                <StyledTableCell>
                  <Button
                    variant="contained"
                    sx={{ bgcolor: "rgb(158, 158, 158) " }}
                    aria-label="edit"
                    onClick={() => this.props.handler(row)}
                  >
                    Select
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

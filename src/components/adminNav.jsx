import * as React from "react";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PeopleIcon from "@mui/icons-material/People";
import HealingIcon from "@mui/icons-material/Healing";
import { Link } from "react-router-dom";

export default function mainListItems() {
  return (
    <div>
      <Link style={{ textDecoration: "none", color: "black" }} to="">
        <ListItem button>
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
      </Link>
      <Link style={{ textDecoration: "none", color: "black" }} to="patients">
        <ListItem button>
          <ListItemIcon>
            <HealingIcon />
          </ListItemIcon>
          <ListItemText primary="Patients" />
        </ListItem>
      </Link>
      <Link style={{ textDecoration: "none", color: "black" }} to="staff">
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary="Staff" />
        </ListItem>
      </Link>
    </div>
  );
}

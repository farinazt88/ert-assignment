import * as React from "react";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardIcon from "@mui/icons-material/Dashboard";
import HealingIcon from "@mui/icons-material/Healing";
import { Link } from "react-router-dom";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import ScienceIcon from "@mui/icons-material/Science";
import EventNoteIcon from "@mui/icons-material/EventNote";
import DynamicFeedIcon from "@mui/icons-material/DynamicFeed";
import ContactMailIcon from "@mui/icons-material/ContactMail";

export default class mainListItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <Link
          style={{
            display: this.state.display,
            textDecoration: "none",
            color: "black",
          }}
          to=""
        >
          <ListItem button sx={{}}>
            <ListItemIcon>
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Patients" />
          </ListItem>
        </Link>

        <Link
          style={{ textDecoration: "none", color: "black" }}
          to="contactDetails"
        >
          <ListItem button>
            <ListItemIcon>
              <ContactMailIcon />
            </ListItemIcon>
            <ListItemText primary="Contact details" />
          </ListItem>
        </Link>
        <Link
          style={{ textDecoration: "none", color: "black" }}
          to="AllergiesMeds"
        >
          <ListItem button>
            <ListItemIcon>
              <HealingIcon />
            </ListItemIcon>
            <ListItemText primary="Allergies and Meds" />
          </ListItem>
        </Link>
        <Link
          style={{ textDecoration: "none", color: "black" }}
          to="labTestRes"
        >
          <ListItem button>
            <ListItemIcon>
              <ScienceIcon />
            </ListItemIcon>
            <ListItemText primary="Lab Test Result" />
          </ListItem>
        </Link>
        <Link style={{ textDecoration: "none", color: "black" }} to="radiology">
          <ListItem button>
            <ListItemIcon>
              <DynamicFeedIcon />
            </ListItemIcon>
            <ListItemText primary="Radiology" />
          </ListItem>
        </Link>
        <Link style={{ textDecoration: "none", color: "black" }} to="notes">
          <ListItem button>
            <ListItemIcon>
              <EventNoteIcon />
            </ListItemIcon>
            <ListItemText primary="notes" />
          </ListItem>
        </Link>
        <Link style={{ textDecoration: "none", color: "black" }} to="billing">
          <ListItem button>
            <ListItemIcon>
              <CreditCardIcon />
            </ListItemIcon>
            <ListItemText primary="billing" />
          </ListItem>
        </Link>
      </div>
    );
  }
}

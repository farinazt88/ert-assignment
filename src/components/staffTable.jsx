import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteIcon from "@mui/icons-material/Delete";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "rgba(0, 0, 0, 0.54)",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function createData(
  patientName,
  birthDate,
  gender,
  healthNumber,
  insuranceProvider
) {
  return { patientName, birthDate, gender, healthNumber, insuranceProvider };
}

const rows = [
  createData("Robert Yun", "February 25, 1994", "male", 59400849, "xy"),
  createData("Louie Neece", "June 22, 1942", "male", 29090333, "xy"),
  createData("Mark R. Elizondo", "August 24, 1937", "male", 79773249, "xy"),
  createData("Wendy L. Moore", "May 4, 1976", "female", 56030360, "xy"),
  createData("Rosalie Brooks", "August 14, 1992", "female", 88230653, "xy"),
];

export default function CustomizedTables() {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell sx={{ bgcolor: "white" }}>
              Patient name
            </StyledTableCell>
            <StyledTableCell>Birth Date</StyledTableCell>
            <StyledTableCell>Gender</StyledTableCell>
            <StyledTableCell>Health Card Number</StyledTableCell>
            <StyledTableCell>Insurance Provider</StyledTableCell>
            <StyledTableCell></StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.patientName}>
              <StyledTableCell component="th" scope="row">
                {row.patientName}
              </StyledTableCell>
              <StyledTableCell>{row.birthDate}</StyledTableCell>
              <StyledTableCell>{row.gender}</StyledTableCell>
              <StyledTableCell>{row.healthNumber}</StyledTableCell>
              <StyledTableCell>{row.insuranceProvider}</StyledTableCell>
              <StyledTableCell>
                <IconButton aria-label="edit">
                  <EditIcon />
                </IconButton>

                <IconButton aria-label="Delete">
                  <DeleteIcon />
                </IconButton>
                <IconButton aria-label="more">
                  <MoreVertIcon />
                </IconButton>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
